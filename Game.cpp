#include <iostream>
#include "Game.h"

unsigned int    randuint(unsigned int max)
{
	return rand() % (max + 1);
}

Game::Game(int e) : _zoom(1), _clicked(0), _paused(false), _ready(false)
{
	_edges = e;
	_cam.x = 0;
	_cam.y = 0;
	setTitle("Starting...");
	//sprintf(_title, "Starting...");
	std::cout << "." << std::flush;
	// creer une liste desordonnee de toutes les coord possibles
	std::cout << "." << std::flush;

	std::cout << "." << std::flush;
}

bool		Game::isReady() const
{
	return this->_ready;
}

bool		Game::paused() const
{
	return this->_paused;
}

char*		Game::getTitle()
{
	return this->_title;
}

long long	Game::getZoom() const
{
	return this->_zoom;
}

void	Game::increaseCam()
{
	this->_cam.x++;
	this->_cam.y++;
}

Coord		Game::getCam() const
{
	return this->_cam;
}

SDL_Surface*	Game::getScreen() const
{
	return this->_screenSurface;
}

bool		Game::belongs(int x, int y)
{
	if (x >= 0 && x < _xdim && y >= 0 && y < _ydim)
		return true;
	return false;
}

void		Game::sync()
{
	//SDL_Flip(this->_screen);
	
	SDL_UpdateTexture(this->_screenTexture, NULL, this->_screenSurface->pixels, this->_screenSurface->pitch);
	SDL_RenderClear(this->_screenRenderer);
	SDL_RenderCopy(this->_screenRenderer, this->_screenTexture, NULL, NULL);
	SDL_RenderPresent(this->_screenRenderer);
}

void		Game::setEdges(int e)
{
	this->_edges = e;
	std::cout << this->_edges << std::endl;
}

int		Game::getEdges() const
{
	return this->_edges;
}

bool		Game::clicked() const
{
	if (this->_clicked)
		return true;
	return false;
}

int		Game::getMx() const
{
	return this->_mouse.x;
}

int		Game::getMy() const
{
	return this->_mouse.y;
}
