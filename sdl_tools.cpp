/*
** sdl_tools.c for project in /u/all/muller_b/cu/travail/c/
**
** Made by quentin muller
** Login   <muller_b@epitech.net>
**
** Started on  Tue Apr  6 10:52:08 2010 quentin muller
** Last update Tue Apr 27 21:44:51 2010 leroy_d
*/

#include <iostream>
#include "utils.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "Game.h"


void			Game::cls()
{
	SDL_SetRenderDrawColor(this->_screenRenderer, 0, 0, 0, 255);
	SDL_RenderClear(this->_screenRenderer);
	SDL_RenderPresent(this->_screenRenderer);
	//SDL_FillRect(SDL_GetVideoSurface(), NULL, SDL_MapRGB(SDL_GetVideoSurface()->format, 0, 0, 0));
}

bool			Game::init_SDL()
{
	//if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		std::cout << "Error SDL : Cant initialize SDL.\n" << std::endl;
		return false;
	}
	if ((SDL_CreateWindowAndRenderer(_xdim, _ydim, /*SDL_WINDOW_SHOWN | SDL_WINDOW_MOUSE_CAPTURE | */SDL_WINDOW_ALLOW_HIGHDPI, &this->_screen, &this->_screenRenderer)) != 0) // SDL_HWSURFACE | SDL_DOUBLEBUF 
	{
		std::cout << "[ERROR][SDL] Failed to create window and renderer." << std::endl;
		return false;
	}
	if ((this->_screenSurface = SDL_CreateRGBSurface(0, _xdim, _ydim, _bpp, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000)) == NULL)
	{
		std::cout << "[ERROR][SDL] Failed to create surface." << std::endl;
		return false;
	}
	if ((this->_screenTexture = SDL_CreateTexture(this->_screenRenderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, _xdim, _ydim)) == NULL)
	{
		std::cout << "[ERROR][SDL] Failed to create texture." << std::endl;
		return false;
	}
	if (TTF_Init() < 0)
	{
		std::cerr << "TTF error : %s\n" << TTF_GetError() << std::endl;
		return false;
	}
	this->font_load();
	this->_options.gaussFilters = createGaussFilters(_options.maxAALevel);
	this->_ready = true;
	return true;
}

void		Game::setTitle(const char* title)
{
	sprintf_s(this->_title, title);
}

void		Game::applyTitle()
{
	SDL_SetWindowTitle(this->_screen, this->_title);
}

int			Game::timer_countdown(int *start, int seconds)
{
	if ((((int)clock() / 10000) - *start) > seconds)
	{
		*start = 0;
		return (0);
	}
	return (1);
}

int			Game::get_timer()
{
	return (((int)clock() / 10000));
}

void			Game::wait()
{
	char			c;

	std::cout << "Push enter to continue..." << std::endl;
	std::cin >> c;
}

// apply blur filter
void			Game::blur()
{
	int			xStep = (_bpp / 8);
	int			yStep = this->_screenSurface->pitch;
	Uint8*		p = ((Uint8 *)this->_screenSurface->pixels) + xStep + yStep;
	Uint8*		pTmp = new Uint8[xStep * _xdim + yStep * _ydim];
	long long	pTmpOfs = pTmp - p;

	for (int currentY = 1; currentY <= _ydim - 1; ++currentY)
	{
		Uint8*	pOrg = p;
		for (int currentX = 1; currentX <= _xdim - 1; ++currentX)
		{
			Uint32	bTotal = *(p) * 4, vTotal = *(p + 1) * 4, rTotal = *(p + 2) * 4;
			for (int xOfs = -1; xOfs <= 1; ++xOfs)
			{
				for (int yOfs = -1; yOfs <= 1; ++yOfs)
				{
					if ((bool)xOfs != (bool)yOfs)
					{
						bTotal += *(p + xOfs * xStep + yOfs * yStep);
						vTotal += *(p + xOfs * xStep + yOfs * yStep + 1);
						rTotal += *(p + xOfs * xStep + yOfs * yStep + 2);
					}
				}
			}
			bTotal /= 8;
			vTotal /= 8;
			rTotal /= 8;
			*(p + pTmpOfs)	 = (Uint8)bTotal;
			*(p + pTmpOfs + 1) = (Uint8)vTotal;
			*(p + pTmpOfs + 2) = (Uint8)rTotal;
			*(p + pTmpOfs + 3) = *(p + 3);
			p += xStep;
		}
		p = pOrg;
		p += yStep;
	}
	
	Uint32*		pR = (Uint32*)pTmp;
	Uint32*		ptR = (Uint32*)this->_screenSurface->pixels;
	for (int currentY = 0; currentY < _ydim; ++currentY)
	{
		for (int currentX = 0; currentX < _xdim; ++currentX)
		{
			*(ptR) = *(pR);
			++pR;
			++ptR;
		}
	}
	delete pTmp;
}

void			Game::square32(Uint16 x, Uint16 xSize, Uint16 y, Uint16 ySize, Uint32 color)
{
	int			xStep = (_bpp / 8);
	int			xMax = (x + xSize);
	int			yStep = this->_screenSurface->pitch;
	int			yMax = (y + ySize);
	Uint8*		p = ((Uint8 *)this->_screenSurface->pixels) + y * yStep + x * xStep;

	if (xMax > _xdim)
		xMax = _xdim;
	if (yMax > _ydim)
		yMax = _ydim;

	for (int currentY = y; currentY < yMax; ++currentY)
	{
		Uint8*	pOrg = p;
		for (int	currentX = x; currentX < xMax; ++currentX)
		{
			*(Uint32 *)p = color;
			p += xStep;
		}
		p = pOrg;
		p += yStep;
	}
}

void			Game::centeredSquare32(Uint16 x, Uint16 xSize, Uint16 y, Uint16 ySize, Uint32 color)
{
	int			xStep = (_bpp / 8);
	int			xMax = (x + xSize);
	int			xMin = (x - xSize);
	int			yStep = this->_screenSurface->pitch;
	int			yMax = (y + ySize);
	int			yMin = (y - ySize);

	if (xMin < 0)
		xMin = 0;
	if (yMin < 0)
		yMin = 0;
	if (xMax > _xdim)
		xMax = _xdim;
	if (yMax > _ydim)
		yMax = _ydim;

	Uint8*		p = ((Uint8 *)this->_screenSurface->pixels) + yMin * yStep + xMin * xStep;


	for (int currentY = yMin; currentY <= yMax; ++currentY)
	{
		Uint8*	pOrg = p;
		for (int currentX = xMin; currentX <= xMax; ++currentX)
		{
			*(Uint32 *)p = color;
			p += xStep;
		}
		p = pOrg;
		p += yStep;
	}
}

void			Game::dot32(Uint16 x, Uint16 y, Uint32 color)
{
	Uint8 * p = ((Uint8 *)this->_screenSurface->pixels) + y * this->_screenSurface->pitch + x * (_bpp / 8);
	*(Uint32 *)p = color;
}

void			Game::dot32(SDL_Surface* dst, Uint16 x, Uint16 y, Uint32 color)
{
	Uint8 * p = ((Uint8 *)dst->pixels) + y * dst->pitch + x * (_bpp / 8);
	*(Uint32 *)p = color;
}

Uint32			Game::getDot32(SDL_Surface* dst, Uint16 x, Uint16 y)
{
	Uint8 * p = ((Uint8 *)dst->pixels) + y * dst->pitch + x * (_bpp / 8);
	return *(Uint32*)p;
}

void			Game::maxDot32(SDL_Surface* dst, Uint16 x, Uint16 y, Uint32 color)
{
	Uint8 * p = ((Uint8 *)dst->pixels) + y * dst->pitch + x * (_bpp / 8);
	Uint8 * pc = ((Uint8 *)&color);
	if (*p < *pc)
		*p = *pc;
	++pc; ++p;
	if (*p < *pc)
		*p = *pc;
	++pc; ++p;
	if (*p < *pc)
		*p = *pc;
	++pc; ++p;
	if (*p < *pc)
		*p = *pc;
}

void			Game::dot(Uint16 x, Uint16 y, Uint32 color)
{
	Uint8 * p = ((Uint8 *)this->_screenSurface->pixels) + y * this->_screenSurface->pitch + x * (_bpp / 8);

	switch (_bpp / 8)
	{
	case 1:
		*p = (Uint8)color;
		break;
	case 2:
		*(Uint16 *)p = (Uint16)color;
		break;
	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
		{
			*(Uint16 *)p = ((color >> 8) & 0xff00) | ((color >> 8) & 0xff);
			*(p + 2) = color & 0xff;
		}
		else
		{
			*(Uint16 *)p = color & 0xffff;
			*(p + 2) = ((color >> 16) & 0xff);
		}
		break;
	case 4:
		*(Uint32 *)p = color;
		break;
	}
}
