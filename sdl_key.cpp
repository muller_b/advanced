#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#ifdef _WIN32
	#include <SDL.h>
	#include <SDL_ttf.h>
#else
	#include <SDL/SDL.h>
	#include <SDL/SDL_ttf.h>
#endif

#include "Game.h"
#ifdef _WIN32
	#include <chrono>
	#include <thread>
	using namespace std::literals::chrono_literals;
#endif

void		Game::cam_moveRatio(int x, int y)
{
	long double	xt = x*(_xdim*1.0 / this->_zoom) / 100 + this->_cam.x;
	long double	yt = y*(_ydim*1.0 / this->_zoom) / 100 + this->_cam.y;

	this->_cam.x = xt;
	this->_cam.y = yt;
	//std::cout << "Cam moving to " << this->_cam.x << " " << this->_cam.y << std::endl;
	_requireFullRedraw = true;
}

void		Game::cam_move(int x, int y)
{
	long double	xt = (x*1.0 / this->_zoom) + this->_cam.x;
	long double	yt = (y*1.0 / this->_zoom) + this->_cam.y;

	this->_cam.x = xt;
	this->_cam.y = yt;
	//std::cout << "Cam moving to " << this->_cam.x << " " << this->_cam.y << std::endl;
	_requireFullRedraw = true;
}

void		Game::applyCameraZoom(Coord& cursorPos, long long newZoom)
{
	/*
	this->_cam.x = (this->_cam.x + cursorPos.x/this->_zoom) - (_xdim / newZoom) / 2;
	this->_cam.y = (this->_cam.y + cursorPos.y/this->_zoom) - (_ydim / newZoom) / 2;
	*/
	if (newZoom == 0)
		return;

	this->_cam.x = (this->_cam.x + cursorPos.x / this->_zoom) - (_xdim / (long double)newZoom) / (_xdim / cursorPos.x);
	this->_cam.y = (this->_cam.y + cursorPos.y / this->_zoom) - (_ydim / (long double)newZoom) / (_ydim / cursorPos.y);
	this->_zoom = newZoom;
	_requireFullRedraw = true;
}

void		Game::modR(Uint8 val)
{
	_options.rOfs += val;
	_requireFullRedraw = true;
}

void		Game::modV(Uint8 val)
{
	_options.vOfs += val;
	_requireFullRedraw = true;
}

void		Game::modB(Uint8 val)
{
	_options.bOfs += val;
	_requireFullRedraw = true;
}

void		Game::switchAA()
{
	_options.AALevel++;
	if (_options.AALevel > _options.maxAALevel)
		_options.AALevel = 0;
	_options.usingRealAA = _options.AALevel;
	_requireFullRedraw = true;
}

void		Game::switchBlur()
{
	_options.usingBlur = !_options.usingBlur;
	_requireFullRedraw = true;
}

void		Game::updateMousePos()
{
	int		x;
	int		y;

	SDL_GetMouseState(&x, &y);
	this->_mouse.x = x;
	this->_mouse.y = y;
}

int		Game::manage_keyboard()
{
	static Coord*	center = NULL;

	if (center == NULL)
	{
		center = new Coord();
		center->x = _xdim / 2;
		center->y = _ydim / 2;
	}
	if (_keystate[SDL_SCANCODE_UP])
		cam_moveRatio(0, -1);
	if (_keystate[SDL_SCANCODE_DOWN])
		cam_moveRatio(0, 1);
	if (_keystate[SDL_SCANCODE_LEFT])
		cam_moveRatio(-1, 0);
	if (_keystate[SDL_SCANCODE_RIGHT])
		cam_moveRatio(1, 0);
	if (_keystate[SDL_SCANCODE_ESCAPE])
		return (0);
	if (_keystate[SDL_SCANCODE_PAGEUP])
		this->applyCameraZoom(*center, this->_zoom * 2);
	if (_keystate[SDL_SCANCODE_PAGEDOWN])
		this->applyCameraZoom(*center, this->_zoom / 2);
	if (_keystate[SDL_SCANCODE_Q])
		switchAA();
	if (_keystate[SDL_SCANCODE_I])
		modR(1);
	if (_keystate[SDL_SCANCODE_K])
		modR(-1);
	if (_keystate[SDL_SCANCODE_O])
		modV(1);
	if (_keystate[SDL_SCANCODE_L])
		modV(-1);
	if (_keystate[SDL_SCANCODE_P])
		modB(1);
	if (_keystate[SDL_SCANCODE_SEMICOLON]) // actual M on azerty
		modB(-1);
	if (_keystate[SDL_SCANCODE_W])
		switchBlur();
	if (_keystate[SDL_SCANCODE_E])
	{
		_options.usingFakeAA = !_options.usingFakeAA;
		_requireFullRedraw = true;
	}
	if (_keystate[SDL_SCANCODE_SPACE])
	{
		if (this->_paused)
			_paused = false;
		else
			_paused = true;
	}
	if (_keystate[SDL_SCANCODE_RETURN])
	{
		if (_edges)
			this->_edges = 0;
		else
			this->_edges = 1;
	}
	return (1);
}

void		Game::manage_mouse(SDL_Event *event)
{
	/* Deplacement mouse drag
	cam_move(-1 * (event->button.x - this->_mouse.x),
		 -1 * (event->button.y - this->_mouse.y));
	*/
	this->_mouse.x = event->button.x;
	this->_mouse.y = event->button.y;
}

int					Game::manage_keys(bool allowKeyRepeat)
{
	static bool		keyDown = false;
	SDL_Event		event;

	//SDL_PumpEvents();
	if (SDL_PollEvent(&event))
	{
		this->updateMousePos();
		if (event.type == SDL_QUIT)
			return (0);
		else if (event.type == SDL_KEYUP || event.type == SDL_KEYDOWN)
		{
			keyDown = true;
			_keystate = SDL_GetKeyboardState(NULL);
		}
		if (event.type == SDL_MOUSEBUTTONDOWN)
		{
			if (event.button.button == SDL_BUTTON_LEFT)
			{
				this->_clicked = 1;
				long double	mousex = this->_cam.x + this->_mouse.x / this->_zoom;
				long double	mousey = this->_cam.y + this->_mouse.y / this->_zoom;
				std::cout << "xmouse=" << mousex << " ymouse=" << mousey << std::endl;
				_mouseLeftDown = this->_mouse;
			}
		}
		else if (event.type == SDL_MOUSEBUTTONUP)
		{
			_mouseLeftDown.x -= this->_mouse.x;
			_mouseLeftDown.y -= this->_mouse.y;
			cam_move(_mouseLeftDown.x, _mouseLeftDown.y);
			this->_clicked = 0;
		}
		if (event.type == SDL_MOUSEWHEEL)
		{
			if (event.wheel.y > 0)	// WHEEL UP
				this->applyCameraZoom(this->_mouse, this->_zoom * 2);
			else if (event.wheel.y < 0)	// WHEEL DOWN
				this->applyCameraZoom(this->_mouse, this->_zoom / 2);
		}
		if (event.type == SDL_MOUSEMOTION && this->_clicked)
		{
			_mouseLeftDown.x -= this->_mouse.x;
			_mouseLeftDown.y -= this->_mouse.y;
			cam_move(_mouseLeftDown.x, _mouseLeftDown.y);
			_mouseLeftDown = this->_mouse;
		}
		//	manage_mouse(&event);
	}
	//std::cout << "keyDown" << keyDown << " allowKeyRepeat" << allowKeyRepeat << "\n";
	if (keyDown && allowKeyRepeat)
		if (!manage_keyboard())
			return (0);
	return (1);
}
