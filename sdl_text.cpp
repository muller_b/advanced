/*
** sdl_tools.c for project in /u/all/muller_b/cu/travail/c/
**
** Made by quentin muller
** Login   <muller_b@epitech.net>
**
** Started on  Tue Apr  6 10:52:08 2010 quentin muller
** Last update Tue Apr  6 10:52:08 2010 quentin muller
*/

#include <iostream>
#ifdef _WIN32
	#include <SDL.h>
	#include <SDL_ttf.h>
#else
	#include <SDL/SDL.h>
	#include <SDL/SDL_ttf.h>
#endif
#include "Game.h"

void			Game::font_load()
{
  this->_font = TTF_OpenFont("Material_Sans.ttf", 12);
  if (!this->_font)
    std::cerr << "TTF error : " << TTF_GetError() << std::endl;
  else
    std::cout << "Material_Sans.ttf loaded." << std::endl;
  return ;
}

void			Game::text(const char *text, SDL_Color* col, int x, int y)
{
  SDL_Surface		*text_img;
  SDL_Rect		pos;

  pos.x = x;
  pos.y = y;
  text_img = TTF_RenderText_Blended(this->_font, text, *col);
  SDL_BlitSurface(text_img, NULL, this->_screenSurface, &pos);
  SDL_FreeSurface(text_img);
}
