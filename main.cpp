#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <math.h>
#include "Game.h"
#include "Thread.hpp"
#include <time.h>
#ifdef _WIN32
	#include <chrono>
	#include <thread>
	using namespace std::literals::chrono_literals;
#endif

static const char*	WINDOW_TITLE = "Advanced";
static const char*	WINDOW_TITLE_UPDATING = "Advanced - (Zoom x%lld, AA x%d) FakeAA:%d";
static const char*	WINDOW_TITLE_REPORT = "Advanced - (Zoom x%lld, AA x%d) FakeAA:%d";

char		_running = 0;

typedef void*	(*ThreadFct)(void*);

static long long										TITLE_REFRESH_PERIOD_US = 100000; // 0.1sec

#ifdef _WIN32
	void*		startKeyManaging(void* ag)
	{
		Game*													game = static_cast<Game*>(ag);
		std::chrono::steady_clock								chrono;
		std::chrono::time_point<std::chrono::steady_clock>		start, end;
		long long												elapsed;
		bool													allowKeyRepeat = true;

		// IDK Why but this has to be called here (eg. initialize SDL in the key-polling thread)
		game->init_SDL();
		game->setTitle(WINDOW_TITLE);

		start = chrono.now();
		while (game->manage_keys(allowKeyRepeat))
		{
			allowKeyRepeat = false;
			end = chrono.now();
			elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
			if (elapsed >= TITLE_REFRESH_PERIOD_US)
			{
				start = end;
				game->_refreshTitle = true;
				game->applyTitle();
				allowKeyRepeat = true;
			}
			std::this_thread::sleep_for(5ms); //5ms = std::chrono::microseconds(5) // thanks to using namespace std::literals::chrono_literals;
		}
		std::cout << "END.\n";
		_running = 2;
		return NULL;
	}
#else
	void*		startKeyManaging(void* ag)
	{
		Game*		game = static_cast<Game*>(ag);
		struct timespec start, finish;
		double elapsed;

		clock_gettime(CLOCK_MONOTONIC, &start);
		while (game->manage_keys())
		{
			clock_gettime(CLOCK_MONOTONIC, &finish);
			elapsed = (finish.tv_sec - start.tv_sec);
			elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
			if (elapsed >= 0.1)
			{
				clock_gettime(CLOCK_MONOTONIC, &start);
				SDL_WM_SetCaption(game->getTitle(), NULL);
			}
			usleep(5000);
		}
		std::cout << "END.\n";
		_running = 2;
		return NULL;
	}
#endif
	int DrawImage(SDL_Surface *surface, char *image_path, int x_pos, int y_pos)
{
	SDL_Surface *image = IMG_Load(image_path);
	if (!image)
	{
		printf("IMG_Load: %s\n", IMG_GetError());
		return 1;
	}

	// Draws the image on the screen:
	SDL_Rect rcDest = { x_pos, y_pos, 0, 0 };
	SDL_BlitSurface(image, NULL, surface, &rcDest);

	// something like SDL_UpdateRect(surface, x_pos, y_pos, image->w, image->h); is missing here

	SDL_FreeSurface(image);
	return 0;
}

#ifdef _WIN32
	int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
	{
		// link stdout to VS console
		AllocConsole();
		FILE* pCout;
		freopen_s(&pCout, "conout$", "w", stdout);
		//freopen_s("CONOUT$", "w", stdout);
		//freopen_s("CONOUT$", "w", stderr);
#else
	int		main(int ac, char** av)
	{
#endif

	// randomize time
	srand(time(NULL));
	std::cout << "Loading";

#ifndef _WIN32
	if (ac < 1)
	{
		std::cout << "Usage: cmd <>" << std::endl;
		return 0;
	}
#endif

	Game		g(1);
	unsigned int	_sleep_time = 0;

	_running = 1;
#ifndef _WIN32
	if (ac > 2)
		_sleep_time = atoi(av[2]);
#endif
	std::cout << std::endl;
	std::cout.setf(std::ios::fixed, std::ios::floatfield);
	std::cout.precision(10);

	AThread	thread;
	thread.start((Routine)(startKeyManaging), static_cast<void*>(&g));

	// OPTIONS
	g._options.usingRealAA = g._options.AALevel;


	// wait for SDL to be ready
	while (!g.isReady()) std::this_thread::sleep_for(5ms);

	long long	pixelCount = _xdim * _ydim;

	bool		flagFirstLoop = true;
	// BOUCLE P
	while (_running != 2)
	{
		//      std::cin >> xzoom;
		//std::cout << "xrmin=" << xrmin << " yrmin=" << yrmin << " xrmax=" << xrmax << " yrmax=" << yrmax << " zoom=" << g.getZoom() << std::endl;

		for (int x = 0; x < _xdim; x++)
		{
			if (g._refreshTitle)
			{
				g._refreshTitle = false;
				sprintf_s(g.getTitle(), g.TITLE_BUFFER_SIZE, WINDOW_TITLE_UPDATING,
					g.getZoom(), g._options.AALevel, g._options.usingFakeAA);
				/*if (g._requireFullRedraw)
					break;*/
				g.sync();
			}

			// @todo: MAKE SO THAT ONLY UNCOMPUTED ONES GET PROCESSED HERE (ZoneSteps)
			for (int y = 0; y < _ydim; y++)
			{
				//
				g.dot32(x, y, SDL_MapRGB(g.getScreen()->format, x%255, y%255, (x+y)%255));
			}	// end of Y LOOP
			while (g.paused() && !(_running == 2))
				std::this_thread::sleep_for(10ms);
		}		// end of X LOOP

		sprintf_s(g.getTitle(), g.TITLE_BUFFER_SIZE, WINDOW_TITLE_REPORT,
			g.getZoom(), g._options.AALevel, g._options.usingFakeAA);
		if (g._options.usingBlur)
			g.blur();
		DrawImage(g.getScreen(), (char *)"E:\\Projets_perso\\advanced\\s.bmp", 100, 100);
		DrawImage(g.getScreen(), (char *)"s.bmp", 100, 100);
		g.sync();
		g._requireFullRedraw = false;

		if (_sleep_time)
			std::this_thread::sleep_for(std::chrono::microseconds(_sleep_time));
		//g.cls();
		if (_verb)
			std::cout << "-----------------Refreshed-----------------" << std::endl;

		while (!g._requireFullRedraw)
		{
			std::this_thread::sleep_for(10ms);
			if (_running == 2)
				return 0;
		}
	}
	return 0;
}
