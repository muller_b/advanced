#include <iostream>
#ifdef _WIN32
	#include <SDL.h>
	#include <SDL_ttf.h>
#else
	#include <SDL/SDL.h>
	#include <SDL/SDL_ttf.h>
#endif
#include "utils.hpp"

int***			createGaussFilters(int maxFilterSize)
{
	int***		gaussFilters = new int**[maxFilterSize];

	for (int i = 0; i < maxFilterSize + 1; ++i)
		gaussFilters[i] = createGaussFilter(i);
	return gaussFilters;
}

int**			createGaussFilter(int filterSize)
{
	int			totalSize = 1 + 2 * filterSize;
	int**		GKernel = new int*[totalSize];
	// intialising standard deviation to 1.0
	double		r, scaler = 0.;

	// generating 5x5 kernel
	for (int x = -filterSize; x <= filterSize; ++x)
	{
		GKernel[x + filterSize] = new int[totalSize];
		for (int y = -filterSize; y <= filterSize; ++y)
		{
			r = sqrt(x*x + y*y);
			double	gauss = ((exp(-(r*r) / filterSize)) / (M_PI * filterSize));
			if (scaler == 0.0)
				scaler = 1 / gauss;
			GKernel[x + filterSize][y + filterSize] = scaler * gauss;
		}
	}
	//debug
	for (int i = 0; i < totalSize; ++i)
	{
		for (int j = 0; j < totalSize; ++j)
			std::cout << GKernel[i][j] << "\t";
		std::cout << std::endl;
	}
	return GKernel;
}
