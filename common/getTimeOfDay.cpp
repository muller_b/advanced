//#include <stdafx.h>
#include <time.h>
#include <windows.h>
#include <iostream>

#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif
 
struct timezone
{
  int  tz_minuteswest;
  int  tz_dsttime;
};
  
int gettimeofday(struct timeval *tv, struct timezone *tz)
{

  FILETIME ft;
 
  unsigned __int64 tmpres = 0;
  static int tzflag = 0;
 
  if (NULL != tv)
  {
    GetSystemTimeAsFileTime(&ft);
 
    tmpres |= ft.dwHighDateTime;
    tmpres <<= 32;
    tmpres |= ft.dwLowDateTime;
 
    tmpres /= 10;
 
    tmpres -= DELTA_EPOCH_IN_MICROSECS;
 
    tv->tv_sec = (long)(tmpres / 1000000UL);
    tv->tv_usec = (long)(tmpres % 1000000UL);
  }
 
  if (NULL != tz)
  {
    if (!tzflag)
    {
      _tzset();
      tzflag++;
    }
  
	long	tzTmp;
	if (_get_timezone(&tzTmp))
		tz->tz_minuteswest = (int)(tzTmp / 60);//_timezone / 60;
	int		dlTmp;
	if (_get_daylight(&dlTmp))
		tz->tz_dsttime = dlTmp;
  }
 
  return 0;
}
    

//Example usage to for interval timing:
/*
int main()
{
  struct timeval timediff;
  char dummychar;
 
// Do some interval timing
  gettimeofday(&timediff, NULL);
  double t1=timediff.tv_sec+(timediff.tv_usec/1000000.0);

// Waste a bit of time here
  cout << "Give me a keystroke and press Enter." << endl;
  cin >> dummychar;
  cout << "Thanks." << endl;
 
//Now that you have measured the user's reaction time, display the results in seconds
  gettimeofday(&timediff, NULL);
  double t2=timediff.tv_sec+(timediff.tv_usec/1000000.0);
  cout << t2-t1 << " seconds have elapsed" << endl;
  return 0;
}
    
*/