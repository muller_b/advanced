#ifndef _ISOCKET_H_
#define _ISOCKET_H_

#include	<iostream>
#ifndef	_WIN32
typedef int	MySocket;
#else
#include <winsock2.h>
#include <windows.h>
//#include <ws2tcpip.h>
typedef SOCKET	MySocket;
#endif

class ISocket
{
public:
  virtual bool		send(const void* data, size_t) = 0;
  virtual bool		recv(void* data, size_t) = 0;
  virtual MySocket	getSocket() const = 0;
};

#endif /* _ISOCKET_H_ */
