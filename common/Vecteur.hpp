#ifndef		_VECTEUR_
#define		_VECTEUR_

#include	<iostream>

class		Vecteur
{
private:
  int		_x;
  int		_y;
public:
  Vecteur();
  Vecteur(int, int);
  Vecteur(const Vecteur&);

  void		setCoor(int&, int&);
  void		setX(int);
  void		setY(int);
  int		getX(void) const;
  int		getY(void) const;

  Vecteur&	operator +=(const Vecteur& right);
  Vecteur&	operator =(const Vecteur&);
  Vecteur&	operator +(const Vecteur&);
  Vecteur&	operator -(const Vecteur&);
};

std::ostream&	operator <<(std::ostream&, const Vecteur&);

#endif
