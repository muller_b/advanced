#ifndef _SOCKETSELECTWIN_H_
#define _SOCKETSELECTWIN_H_

#include "ASocketSelect.hpp"

class SocketSelectWin : public ASocketSelect
{
	SocketSelectWin() {};
	SocketSelectWin(SocketSelectWin&) {};
	~SocketSelectWin() {};
public:
	static ASocketSelect*	getInst();
	void			addSocket(const ISocket* sock);
	void			rmSocket(const ISocket* sock);
	bool			select();
	bool			isReadable(const ISocket* sock) const;
	void			pSocks() const;
};

typedef SocketSelectWin SocketSelect;

#endif /* _SOCKETSELECTWIN_H_ */
