#include <Windows.h>
#include "WindowsDLL.hpp"

ISharedLib::dl		WindowsDLL::ldl(const std::string& file)
{
	return (static_cast<dl>(LoadLibraryA(static_cast<LPCSTR>(file.data()))));
}

ISharedLib::result	WindowsDLL::fdl(dl lib)
{
	return (static_cast<result>(FreeLibrary(static_cast<HMODULE>(lib))));
}

ISharedLib::sym		WindowsDLL::getSym(dl lib, const std::string& name)
{
	return (static_cast<sym>(GetProcAddress(static_cast<HMODULE>(lib), static_cast<LPCSTR>(name.data()))));
}
