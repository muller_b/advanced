#include	"AEvent.hpp"

void			AEvent::setTime(const Timer& tmp)
{
  this->_time = tmp.getTime();
}

const struct timeval&	AEvent::getTime() const
{
  return this->_time;
}

char const*		AEvent::getName(void)
{
  return this->_name;
}

void			AEvent::setName(char const* Name)
{
  this->_name = Name;
}

char const*		AEvent::getLogin(void)
{
  return this->_login;
}

void			AEvent::setLogin(char const* Login)
{
  this->_login = Login;
}

char const*		AEvent::getGame(void)
{
  return this->_game;
}

void			AEvent::setGame(char const* Game)
{
  this->_game = Game;
}


AEvent::types		AEvent::getType(void) const
{
  return this->_type;
}

void			AEvent::setType(AEvent::types datype)
{
  this->_type = datype;
}

int			AEvent::getIdObj(void) const
{
  return this->_idObj;
}

void			AEvent::setIdObj(int obj)
{
  this->_idObj = obj;
}

int			AEvent::getAgree(void) const
{
  return this->_agree;
}

void			AEvent::setAgree(int agree)
{
  this->_agree = agree;
}

int			AEvent::getIdGame(void) const
{
  return this->_idGame;
}

void			AEvent::setIdGame(int idg)
{
  this->_idGame = idg;
}

Vecteur const&		AEvent::getVitesse(void) const
{
  return this->_vitesse;
}

void			AEvent::setVitesse(const Vecteur& v)
{
  this->_vitesse = v;
}

void			AEvent::setPos(const Vecteur& p)
{
  this->_pos = p;
}

Vecteur const&		AEvent::getPos(void) const
{
  return this->_pos;
}

void			AEvent::setTypeObj(AEvent::objTypes objT)
{
  this->_typesObj = objT;
}

AEvent::objTypes	AEvent::getTypeObj() const
{
  return this->_typesObj;
}
