#ifndef _APISOCKETUDP_H_
#define _APISOCKETUDP_H_

#include "ISocket.hpp"
#include "ASocketSelect.hpp"

class APISocketUDP : public ISocket
{
public:
  virtual bool	bind(const unsigned int port) = 0;
  virtual void	setDest(const std::string& ip, const unsigned int port) = 0;
  virtual void	setDest(const struct sockaddr_in* sinfo) = 0;
};


#endif /* _APISOCKETUDP_H_ */
