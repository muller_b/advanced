#include	"ThreadUnix.hpp"

bool		ThreadUnix::start(Routine root, void* arg)
{
  pthread_t*	AThread =  new pthread_t;
  int		retour = pthread_create(AThread, 0, root, arg);

  this->_handle = AThread;
  return (retour == 0);
}

void		ThreadUnix::Join()
{
  void		**thread_return = 0;

  pthread_join(*(this->_handle), thread_return);
}

void		ThreadUnix::Exit()
{
  void		*retval = 0;

  pthread_exit(retval);
}
