#include	"AObject.hpp"

AObject::AObject(int id) : _id(id), _life(100)
{

}

AObject::~AObject()
{

}

void		AObject::setType(int type)
{
  this->_type = type;
}

int		AObject::getType() const
{
  return this->_type;
}

void		AObject::collision(AObject* AO)
{
  ++AO;
}

void		AObject::keepDir()
{
  this->deplacement(this->_dir);
}

void		AObject::setDir(int x, int y)
{
  this->_dir.setX(x);
  this->_dir.setY(y);
}

void		AObject::deplacement(const Vecteur& v)
{
  this->_pos += v;
}

void		AObject::deplacement(int x, int y)
{
  this->_pos.setX(this->_pos.getX() + x);
  this->_pos.setY(this->_pos.getY() + y);
}

Vecteur const&	AObject::getDim() const
{
  return (this->_dim);
}

Vecteur const&	AObject::getPos() const
{
  return (this->_pos);
}

Vecteur const&	AObject::getDir() const
{
  return (this->_dir);
}

void		AObject::setDim(int x, int y)
{
  this->_dim.setX(x);
  this->_dim.setY(y);
}

void		AObject::setPos(int x, int y)
{
  this->_pos.setX(x);
  this->_pos.setY(y);
}

void		AObject::setDir(const Vecteur v)
{
  this->_dir.setX(v.getX());
  this->_dir.setY(v.getY());
}

void		AObject::setDim(const Vecteur v)
{
  this->_dim.setX(v.getX());
  this->_dim.setY(v.getY());
}

void		AObject::setPos(const Vecteur v)
{
  this->_pos.setX(v.getX());
  this->_pos.setY(v.getY());
}

int		AObject::getId() const
{
  return (this->_id);
}

void		AObject::setId(int id)
{
  this->_id = id;
}

int		AObject::getLv() const
{
  return (this->_lv);
}

int		AObject::getLife() const
{
  return (this->_life);
}

void		AObject::setLv(int lv)
{
  this->_lv = lv;
}

void		AObject::setLife(int life)
{
  this->_life = life;
}
