#ifndef _ASOCKETSELECT_H_
#define _ASOCKETSELECT_H_

#include	"ISocket.hpp"
#ifdef		_WIN32
#include	<windows.h>
#include	<time.h>
#else
#include	<sys/select.h>
#include	<sys/time.h>
#include	<sys/types.h>
#include	<unistd.h>
#endif
#include <list>

class ASocketSelect
{
protected:
  static ASocketSelect*	_inst;
  MySocket		_maxfd;
  std::list<MySocket>	_sockList;
  fd_set		_fds;
  timeval	_tv;

  ASocketSelect() : _maxfd(3)
  {
    _tv.tv_sec = 0;
    _tv.tv_usec = 1;
  };
public:
  virtual void		addSocket(const ISocket* sock) = 0;
  virtual void		rmSocket(const ISocket* sock) = 0;
  virtual bool		select() = 0;
  virtual bool		isReadable(const ISocket* sock) const = 0;
  virtual void		pSocks() const = 0;
};

#endif /* _ASOCKETSELECT_H_ */
