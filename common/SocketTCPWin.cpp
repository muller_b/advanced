#include		"SocketTCPWin.hpp"

SocketTCPWin::SocketTCPWin() : _ini(false)
{
	int			err;
	//  struct protoent*	proto;

	  /*
	  if ((proto = getprotobyname("tcp")) == NULL)
		{
		  std::cerr << "getprotobyname() failed." << std::endl;
		  return ;
		}
	  */
	this->_wVersion = MAKEWORD(2, 2);
	err = WSAStartup(this->_wVersion, &this->_wsaData);
	if (err != 0)
	{
		std::cerr << "WSAStartup failed with error :" << err << std::endl;
		return;
	}
	if (LOBYTE(this->_wsaData.wVersion) != 2 || HIBYTE(this->_wsaData.wVersion) != 2)
	{
		WSACleanup();
		return;
	}
	this->_sock = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (this->_sock == INVALID_SOCKET)
	{
		std::cerr << WSAGetLastError() << std::endl;
		WSACleanup();
		return;
	}
	if ((this->_sinfo = new struct sockaddr_in) == NULL)
	{
		std::cerr << "new(struct sockaddr_in) failed." << std::endl;
		return;
	}
	this->_ini = true;
}

SocketTCPWin::~SocketTCPWin()
{
	if (closesocket(this->_sock) == SOCKET_ERROR)
		std::cerr << WSAGetLastError() << std::endl;
	if (WSACleanup() != 0)
		std::cerr << WSAGetLastError() << std::endl;
	// Add rmSocket() ?
}

SocketTCPWin::SocketTCPWin(struct sockaddr_in* siadr, int sock) : _ini(false), _sock(sock), _sinfo(siadr)
{
	//(SocketSelectWin::getInst())->addSocket(this);
}


bool				SocketTCPWin::connected() const
{
	return false;
}

bool			SocketTCPWin::listen(const unsigned int port, const int maxcon)
{
	if (this->_ini == true)
	{
		this->_sinfo->sin_family = AF_INET;
		this->_sinfo->sin_addr.s_addr = htonl(INADDR_ANY);
		this->_sinfo->sin_port = htons(port);
		if (bind(this->_sock, (SOCKADDR*)&_sinfo, sizeof(this->_sinfo)) == SOCKET_ERROR)
		{
			std::cerr << "bind() failed" << std::endl;
			closesocket(this->_sock);
			return false;
		}
		if (listen(this->_sock, SOMAXCONN) == false)
			std::cerr << "Error listening on socket." << std::endl;
		std::cout << "Listening on socket..." << std::endl;
		return true;
	}
	return false;
}

bool			SocketTCPWin::connect(const unsigned int port, const std::string& ip)
{
	if (this->_ini)
	{
		this->_sinfo->sin_family = AF_INET;
		this->_sinfo->sin_port = htons(port);
		inet_pton(this->_sinfo->sin_family, ip.c_str(), &(this->_sinfo->sin_addr.s_addr));
		memset(&(this->_sinfo->sin_zero), 0, 8);
		//bzero(&(this->_sinfo->sin_zero), 8);
		if (WSAConnect(this->_sock, (struct sockaddr*)this->_sinfo, sizeof(struct sockaddr), NULL, NULL, NULL, NULL) == SOCKET_ERROR)
		{
			std::cerr << "Cant connect to " << ip.data() << ":" << port << std::endl;
			std::cerr << WSAGetLastError() << std::endl;
			return false;
		}
		return true;
	}
	return false;
}

// Send & Recv doubts on flags

bool			SocketTCPWin::send(const void* data, size_t size)
{
	DWORD temp = size;
	if (WSASend(this->_sock, *static_cast<const LPWSABUF*>(data), 1, &temp, 0, 0, 0) == SOCKET_ERROR)
	{
		std::cerr << WSAGetLastError() << std::endl;
		return false;
	}
	return true;
}

bool			SocketTCPWin::recv(void* data, size_t size)
{
	DWORD temp = size;
	if (WSARecv(this->_sock, *static_cast<LPWSABUF*>(data), 1, &temp, 0, 0, 0) == SOCKET_ERROR)
	{
		std::cerr << WSAGetLastError() << std::endl;
		return false;
	}
	return true;
}

APISocketTCP*		SocketTCPWin::accept()
{
	int                   sock;
	int             unused;
	struct sockaddr_in*   siadr;
	APISocketTCP*         ntcps;

	if ((siadr = new struct sockaddr_in) == NULL)
	{
		std::cerr << "new(struct sockaddr_in) failed." << std::endl;
		return NULL;
	}
	unused = sizeof(struct sockaddr);
	if ((sock = WSAAccept(this->_sock, (struct sockaddr*)siadr, &unused, NULL, NULL)) != INVALID_SOCKET)
	{
		ntcps = new SocketTCPWin(siadr, sock);
		return ntcps;
	}
	std::cerr << WSAGetLastError() << std::endl;
	return NULL;
}

MySocket		SocketTCPWin::getSocket() const
{
	return this->_sock;
}

struct sockaddr_in*     SocketTCPWin::getDest() const
{
	return this->_sinfo;
}
