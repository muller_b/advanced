#ifndef					THREADWINDOWS_H
# define				THREADWINDOWS_H
#include				"APIThread.hpp"

class					ThreadWindows : public APIThread
{
public:
	bool				start(Routine root, void *arg);
	void				Join();
	void				Exit();
private:
	MyHandle			_handle;
};

typedef	ThreadWindows AThread;

#endif
