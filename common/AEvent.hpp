#ifndef				_IEVENT_H_
#define				_IEVENT_H_

#include			<iostream>
#include			<string>
#ifdef				_WIN32
#include			<winsock2.h>
#include			<windows.h>
#else
#include			<sys/time.h>
#endif
#include			"Timer.hpp"
#include			"Vecteur.hpp"

#define				PACKET_SIZE 512

typedef	char			Packet;

class				AEvent
{
public:
  enum	types
    {
      NONE,
      MOVE, /**/
      CREATE_GAME, /**/
      POP_MOB,
      AGREE, /**/
      COLLISION,
      DESTRUCTION,
      JOIN_GAME, /**/
      LOGIN,
      ONLINE,
      FIRE
    };
  enum  objTypes
    {
      UNKNOWN,
      PLAYER,
      BASIC_FIRE,
      STRONG_FIRE1,
      STRONG_FIRE2,
      STRONG_FIRE3,
      STRONG_FIRE4,
      MODULE,
      BASIC_MOB,
      EXPLOSION
    };

protected:
  types				_type;
  int				_idObj;
  int				_idGame;
  objTypes			_typesObj;
  int				_agree;
  Vecteur			_vitesse;
  Vecteur			_pos;
  char const*			_name;
  char const*			_login;
  char const*			_game;
  struct timeval		_time;

public:
  virtual void			setPacket(const Packet*) = 0;
  virtual Packet*		toPacket(void) const = 0;
  virtual char const*		getName(void);
  virtual char const*		getLogin(void);
  virtual char const*		getGame(void);
  virtual AEvent::types		getType(void) const;
  virtual int			getIdObj(void) const;
  virtual int			getAgree(void) const;
  virtual int			getIdGame(void) const;
  virtual Vecteur const&	getVitesse(void) const;
  virtual Vecteur const&	getPos(void) const;
  virtual AEvent::objTypes	getTypeObj() const;
  virtual void			setName(char const*);
  virtual void			setLogin(char const*);
  virtual void			setGame(char const*);
  virtual void			setType(AEvent::types);
  virtual void			setIdObj(int);
  virtual void			setAgree(int);
  virtual void			setIdGame(int);
  virtual void			setVitesse(const Vecteur&);
  virtual void			setPos(const Vecteur&);
  virtual void			setTypeObj(AEvent::objTypes);
  virtual void			setTime(const Timer&);
  virtual const struct timeval&	getTime() const;
  virtual ~AEvent(void){};
};

#endif				/* _IEVENT_H_ */
