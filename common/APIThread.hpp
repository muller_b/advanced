#ifndef				_ABSTRAC_H_
#define				_ABSTRAC_H_

#include			<iostream>
#include			<string>

#ifdef				_WIN32
#include			<windows.h>
typedef	LPTHREAD_START_ROUTINE	Routine;
typedef	HANDLE			MyHandle;
typedef	DWORD			ThreadReturn;
#else
# include			<pthread.h>
typedef	void*			(*Routine)(void*);
typedef	pthread_t		MyHandle;
typedef	void*			ThreadReturn;
#endif

class				APIThread
{
public:
  virtual bool			start(Routine root, void *arg) = 0;
  virtual void			Join() = 0;
  virtual void			Exit() = 0;
  virtual ~APIThread(){};
};

#endif				/* _ABSTRAC_H_ */
