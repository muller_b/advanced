#ifndef _SOCKETSELECTUNIX_H_
#define _SOCKETSELECTUNIX_H_

#include "ASocketSelect.hpp"

class SocketSelectUnix : public ASocketSelect
{
  SocketSelectUnix() {};
  SocketSelectUnix(SocketSelectUnix&) {};
  ~SocketSelectUnix() {};
public:
  static ASocketSelect*	getInst();
  void			addSocket(const ISocket* sock);
  void			rmSocket(const ISocket* sock);
  bool			select();
  bool			isReadable(const ISocket* sock) const;
  void			pSocks() const;
};

typedef SocketSelectUnix SocketSelect;

#endif /* _SOCKETSELECTUNIX_H_ */
