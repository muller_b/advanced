#include	"MutexWin.hpp"

MutexWindows::MutexWindows()
{
  InitializeCriticalSection(&_myMutex);
}

MutexWindows::~MutexWindows()
{
  DeleteCriticalSection(&_myMutex);
}

bool			MutexWindows::trylockMutex()
{
  return (TryEnterCriticalSection(&_myMutex) == 0);
}

bool			MutexWindows::unlockMutex()
{
  LeaveCriticalSection(&_myMutex);
  return true;
}

bool			MutexWindows::lockMutex()
{
  EnterCriticalSection(&_myMutex);
  return true;
}

