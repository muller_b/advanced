#include	"ThreadWindows.hpp"

bool					ThreadWindows::start(Routine root, void* arg)
{
	this->_handle = CreateThread(NULL, 0, root, arg, 0, 0);
	if (this->_handle == NULL)
		return false;
	return true;
}

void					ThreadWindows::Join()
{
	DWORD				success;

	success = WaitForSingleObject(this->_handle, INFINITE);
}

void					ThreadWindows::Exit()
{
	DWORD					exitParam = 0;

	ExitThread(exitParam);
}
