#include "SocketUDPUnix.hpp"
#include "SocketSelectUnix.hpp"

MySocket	SocketUDPUnix::getSocket() const
{
  return this->_sock;
}

SocketUDPUnix::SocketUDPUnix(struct sockaddr_in* sinfo) : _ini(false), _sinfo(sinfo)
{
  struct protoent*	proto;

  if ((proto = getprotobyname("udp")) == NULL)
    {
      std::cerr << "[WARNING][UDP] getprotobyname() failed." << std::endl;
      return ;
    }
  if ((this->_sock = socket(AF_INET, SOCK_DGRAM, proto->p_proto)) == -1)
    {
      std::cerr << "[WARNING][UDP] socket() creation failed." << std::endl;
      return ;
    }
  (SocketSelectUnix::getInst())->addSocket(this);
  this->_ini = true;
}

SocketUDPUnix::SocketUDPUnix() : _ini(false)
{
  struct protoent*	proto;

  if ((proto = getprotobyname("udp")) == NULL)
    {
      std::cerr << "[WARNING][UDP] getprotobyname() failed." << std::endl;
      return ;
    }
  if ((this->_sock = socket(AF_INET, SOCK_DGRAM, proto->p_proto)) == -1)
    {
      std::cerr << "[WARNING][UDP] socket() creation failed." << std::endl;
      return ;
    }
  (SocketSelectUnix::getInst())->addSocket(this);
  if ((this->_sinfo = new struct sockaddr_in) == NULL)
    {
      std::cerr << "[WARNING][UDP] new(struct sockaddr_in) failed." << std::endl;
      return ;
    }
  this->_ini = true;
}

SocketUDPUnix::~SocketUDPUnix()
{
  if (close(this->_sock) == -1)
    std::cerr << "[WARNING][UDP] close(socket) failed." << std::endl;
  (SocketSelectUnix::getInst())->rmSocket(this);
}

bool		SocketUDPUnix::send(const void* data, size_t size)
{
  if ((::sendto(this->_sock, data, size, MSG_DONTWAIT, (struct sockaddr*)this->_sinfo, sizeof(struct sockaddr))) == -1)
    {
      std::cerr << "[WARNING][UDP] send() failed." << std::endl;
      return false;
    }
  return true;
}
#include <errno.h>

bool		SocketUDPUnix::recv(void* data, size_t size)
{
  socklen_t	len = sizeof(struct sockaddr);

  if ((::recvfrom(this->_sock, data, size, MSG_DONTWAIT, (struct sockaddr*)this->_senderinfo, &len)) == -1)
    {
      //      std::cerr << "[WARNING][UDP] recv() failed" << std::endl;
      return false;
    }
  return true;
}

bool		SocketUDPUnix::bind(const unsigned int port)
{
  if (this->_ini)
    {
      this->_sinfo->sin_family = AF_INET;
      this->_sinfo->sin_port = htons(port);
      this->_sinfo->sin_addr.s_addr = htonl(INADDR_ANY);
      bzero(&(this->_sinfo->sin_zero), 8);
      
      if (::bind(this->_sock, (struct sockaddr*)this->_sinfo, sizeof(struct sockaddr)) == -1)
	{
	  std::cerr << "[WARNING][UDP] bind() failed on port " << port << std::endl;
	  return false;
	}
    }
  return true;
}

void		SocketUDPUnix::setDest(const std::string& ip, const unsigned int port)
{
  this->_sinfo->sin_family = AF_INET;
  this->_sinfo->sin_port = htons(port);
  this->_sinfo->sin_addr.s_addr = inet_addr(ip.c_str());
  bzero(&(this->_sinfo->sin_zero), 8);
}

void		SocketUDPUnix::setDest(const struct sockaddr_in* sinfo)
{
  bcopy(sinfo, this->_sinfo, sizeof(struct sockaddr_in));
}

struct sockaddr_in*	SocketUDPUnix::getSender() const
{
  return this->_senderinfo;
}
