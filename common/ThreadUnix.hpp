#ifndef		_THREADUNIX_H_
#define		_THREADUNIX_H_
#include	"APIThread.hpp"

class		ThreadUnix : public APIThread
{
public:
  ThreadUnix() : _handle(0){};
  virtual ~ThreadUnix(){};
  virtual bool	start(Routine root, void *arg);
  virtual void	Join();
  virtual void	Exit();
private:
  MyHandle*	_handle;
};

typedef		ThreadUnix AThread;

#endif		/* _THREADUNIX_H_ */
