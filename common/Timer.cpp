#include <iostream>
#include <sstream>
#include "Timer.hpp"

Timer::Timer()
{
  _tzone.tz_minuteswest = 60;
  _tzone.tz_dsttime = 0;
  gettimeofday(&_time, &_tzone);
}

void		Timer::start()
{
  gettimeofday(&this->_time, &this->_tzone);
}

const struct timeval*	Timer::stop()
{
  gettimeofday(&this->_tmp, &this->_tzone);
  this->_tmp.tv_sec -= this->_time.tv_sec;
  this->_tmp.tv_usec -= this->_time.tv_usec;
  return &this->_tmp;
}

bool		Timer::elapsed(long sec, long usec)
{
  long		secs;

  gettimeofday(&this->_tmp, &this->_tzone);
  secs = this->_tmp.tv_sec - this->_time.tv_sec;
  if (secs > sec)
    return true;
  if ((this->_tmp.tv_usec - this->_time.tv_usec) > usec && secs == sec)
    return true;
  return false;
}
/*
void		Timer::waitUntil(long sec, long usec)
{
  long		secs;
  long		usecs;

  gettimeofday(&this->_tmp, &this->_tzone);
  secs = sec - (this->_tmp.tv_sec - this->_time.tv_sec);
  usecs = usec - (this->_tmp.tv_usec - this->_time.tv_usec);
  if (secs > 0)
    sleep(secs);
  if (usecs > 0)
    usleep(usecs);
}
*/
void		Timer::setTime(struct timeval& timeto)
{
  this->_time.tv_sec = timeto.tv_sec;
  this->_time.tv_usec = timeto.tv_usec;
}

const struct timeval&	Timer::getTime() const
{
  return this->_time;
}

const std::string	Timer::getElapsed()
{
  std::stringstream	time;
  long			ms;

  gettimeofday(&this->_tmp, &this->_tzone);
  this->_tmp.tv_sec -= this->_time.tv_sec;
  this->_tmp.tv_usec -= this->_time.tv_usec;
  ms = this->_tmp.tv_usec / 1000;

  if (this->_tmp.tv_sec)
    {
      time << this->_tmp.tv_sec;
      time << "sec ";
    }
  if (ms)
    {
      time << ms;
      time << "ms ";
    }
  ms = this->_tmp.tv_usec - ms * 1000;
  time << ms;
  time << "us";
  return time.str();
}
