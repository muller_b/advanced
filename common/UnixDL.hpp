#ifndef		__UNIXDL_H__
# define	__UNIXDL_H__

#include <dlfcn.h>
#include "ISharedLib.hpp"

class		UnixDL : public ISharedLib
{
public:
	dl	ldl(const std::string&);
	result	fdl(dl);
	sym	getSym(dl, const std::string&);
};

#endif
