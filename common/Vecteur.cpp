#include	"Vecteur.hpp"

Vecteur::Vecteur(void) : _x(0), _y(0)
{}

Vecteur::Vecteur(int x, int y) : _x(x), _y(y)
{}

Vecteur::Vecteur(const Vecteur& ref)
{
  *this = ref;
}

void		Vecteur::setCoor(int& x, int& y)
{
  this->_x = x;
  this->_y = y;
  return;
}

void		Vecteur::setX(int x)
{
  this->_x = x;
  return;
}

void		Vecteur::setY(int y)
{
  this->_y = y;
  return;
}

int		Vecteur::getX(void) const
{
  return this->_x;
}

int		Vecteur::getY(void) const
{
  return this->_y;
}

Vecteur&	Vecteur::operator =(const Vecteur& right)
{
  if (this == &right)
    return *this;
  this->_x = right._x;
  this->_y = right._y;
  return *this;
}

Vecteur&	Vecteur::operator +(const Vecteur& right)
{
  this->_x += right._x;
  this->_y += right._y;
  return *this;
}

Vecteur&	Vecteur::operator -(const Vecteur& right)
{
  this->_x -= right._x;
  this->_y -= right._y;
  return *this;
}

std::ostream&	operator <<(std::ostream& os, const Vecteur& right)
{
  os << "Position en x : " << right.getX() << std::endl;
  os << "Position en y : " << right.getY() << std::endl;
  return os;
}

Vecteur&Vecteur::operator +=(const Vecteur& right)
{
  *this = *this + right;
  return *this;
}
