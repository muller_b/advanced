#ifndef			SOCKETTCPWIN_H__
#define			SOCKETTCPWIN_H__

#include	       	"APISocketTCP.hpp"
#include			<winsock2.h>
#include			<windows.h>
#include			<ws2tcpip.h>
#include			<stdio.h>


class					SocketTCPWin : public APISocketTCP
{
  SOCKET       			_sock;
  bool					_ini;
  struct sockaddr_in*  	_sinfo;
  WORD					_wVersion;
  WSADATA      			_wsaData;
  SocketTCPWin(struct sockaddr_in* siadr, int sock);
  

public:
  SocketTCPWin();
  ~SocketTCPWin();

  bool					listen(const unsigned int port, const int maxcon);
  bool					connect(const unsigned int port, const std::string& ip);
  bool					send(const void* data, size_t);
  bool					connected(void) const;
  bool					recv(void* data, size_t);
  APISocketTCP*			accept();
  MySocket				getSocket() const;
  struct sockaddr_in*	getDest() const;
};

typedef SocketTCPWin ASocketTCP;

#endif			//SOCKETTCPWIN_H
