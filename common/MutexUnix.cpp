#include		"MutexUnix.hpp"

MutexUnix::MutexUnix()
{
  if (pthread_mutex_init(&_myMutex, NULL) != 0)
    throw new std::runtime_error("Bad Mutex Initialization");
}

MutexUnix::~MutexUnix()
{
  if (pthread_mutex_destroy(&_myMutex) != 0)
    std::cerr << "Bad Mutex Delete" << std::endl;
}

bool			MutexUnix::trylockMutex()
{
  if (pthread_mutex_trylock(&_myMutex) == 0)
    return true;
  return false;
}

bool			MutexUnix::lockMutex()
{
  if (pthread_mutex_lock(&_myMutex) == 0)
    return true;
  return false;
}

bool			MutexUnix::unlockMutex()
{
  if (pthread_mutex_unlock(&_myMutex) == 0)
    return true;
  return false;
}
