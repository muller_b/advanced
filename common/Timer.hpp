#ifndef _TIMER_H_
#define _TIMER_H_

#include <iostream>

#ifdef	_WIN32
#include	<windows.h>
#include	<time.h>
struct timezone
{
  int  tz_minuteswest; /* minutes W of Greenwich */
  int  tz_dsttime;     /* type of dst correction */
};
int gettimeofday(struct timeval *tv, struct timezone *tz);
#else
#include <sys/time.h>
#endif

//Frame duration in microseconds needed to stay at 60 FPS is about 15000
const long	FRAME_LENGTH = 22000;
const long	NORMAL_FRAME_LENGTH = 15000;
//max speed is 100 FPS
const long	MIN_FRAME_LENGTH = 8000;
//dont overload the CPU
const long	SLEEP_LENGTH = 2000;

class Timer
{
  struct timeval	_time;
  struct timezone	_tzone;
  struct timeval	_tmp;
public:
  Timer();
  virtual ~Timer() {};

  void			start();
  const struct timeval*	stop();
  const std::string	getElapsed();
  bool			elapsed(long sec, long usec);
  //void			waitUntil(long sec, long usec);
  void			setTime(struct timeval&);
  const struct timeval&	getTime() const;
};

#endif /* _TIMER_H_ */
