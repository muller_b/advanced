#ifndef SOCKETUDPWIN_H__
#define SOCKETUDPWIN_H__

#include "APISocketUDP.hpp"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <windows.h>

class SocketUDPWin : public APISocketUDP
{
  int                   _sock;
  bool                  _ini;
  struct sockaddr_in*   _sinfo;
  struct sockaddr_in*   _senderinfo;
  WORD			_wVersion;
  WSADATA		_wsaData;

public:
  SocketUDPWin(struct sockaddr_in* sinfo);
  SocketUDPWin();
  ~SocketUDPWin();

  void          setDest(const std::string& ip, const unsigned int port);
  void          setDest(const struct sockaddr_in* sinfo);
  struct sockaddr_in*   getSender() const;

  bool          send(const void* data, size_t);
  bool          recv(void* data, size_t);
  MySocket      getSocket() const;
  bool          bind(const unsigned int port);
};

typedef SocketUDPWin ASocketUDP;

#endif /* _SOCKETUDPWIN_H_ */
