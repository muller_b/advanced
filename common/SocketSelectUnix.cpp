#include "SocketSelectUnix.hpp"

void		SocketSelectUnix::pSocks() const
{
  std::list<MySocket>::const_iterator	it = this->_sockList.begin();
  std::list<MySocket>::const_iterator	ite = this->_sockList.end();

  std::cout << "Sockets in use:";
  for (; it != ite; ++it)
    std::cout << " " << *it;
  std::cout << std::endl;
}

void		SocketSelectUnix::addSocket(const ISocket* sock)
{
  MySocket	s = sock->getSocket();

  if (s >= this->_maxfd)
    this->_maxfd = s + 1;
  this->_sockList.push_front(s);
}

ASocketSelect*	SocketSelectUnix::getInst()
{
  if (!_inst)
    _inst = new SocketSelectUnix;
  return _inst;
}

bool		SocketSelectUnix::select()
{
  std::list<MySocket>::const_iterator	it = this->_sockList.begin();
  std::list<MySocket>::const_iterator	ite = this->_sockList.end();

  FD_ZERO(&this->_fds);
  for (; it != ite; ++it)
    FD_SET(*it, &this->_fds);
  if (::select(this->_maxfd, &this->_fds, NULL, NULL, &this->_tv) == -1)
    return false;
  return true;
}

void		SocketSelectUnix::rmSocket(const ISocket* sock)
{
  std::list<MySocket>::iterator	it = this->_sockList.begin();
  std::list<MySocket>::iterator	ite = this->_sockList.end();
  MySocket	s = sock->getSocket();

  for (; it != ite; ++it)
    {
      if (*it == s)
	{
	  this->_sockList.erase(it);
	  break;
	}
    }
  if (s == this->_maxfd - 1)
    {
      MySocket	newmax = 0;

      std::list<MySocket>::const_iterator	cit = this->_sockList.begin();
      std::list<MySocket>::const_iterator	cite = this->_sockList.end();
      for (; cit != cite; ++cit)
	if (*cit > newmax)
	  newmax = *cit;
      this->_maxfd = newmax + 1;
    }
}

bool		SocketSelectUnix::isReadable(const ISocket* sock) const
{
  if (sock)
    {
      if (FD_ISSET(sock->getSocket(), &this->_fds))
	return true;
    }
  else
    std::cerr << "sock null" << std::endl;
  return false;
}
