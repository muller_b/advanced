#ifndef _MUTEX_H_
#define _MUTEX_H_

#ifdef			_WIN32
# include		"MutexWin.hpp"
#else
# include		"MutexUnix.hpp"
#endif			/*_WIN32*/

#endif /* _MUTEX_H_ */
