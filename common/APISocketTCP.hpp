#ifndef _APISOCKETTCP_H_
#define _APISOCKETTCP_H_

#include "ISocket.hpp"
#include "ASocketSelect.hpp"

class APISocketTCP : public ISocket
{
public:
  virtual bool		listen(const unsigned int port, const int maxcon) = 0;
  virtual bool		connect(const unsigned int port, const std::string& ip) = 0;
  virtual APISocketTCP*	accept() = 0;
  virtual struct sockaddr_in*	getDest() const = 0;
  virtual bool		connected() const = 0;
};

#endif /* _APISOCKETTCP_H_ */
