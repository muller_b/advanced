#ifndef				ABSTRACTMUTEX_HPP__
#define				ABSTRACTMUTEX_HPP__

#include			<iostream>
#include			<string>

#ifdef				_WIN32
# include			<windows.h>
typedef CRITICAL_SECTION       	mutex;
#else
# include			<pthread.h>
typedef	pthread_mutex_t		mutex;
#endif

class				APIMutex
{
public:
  virtual ~APIMutex(){};
  virtual bool			lockMutex() = 0;
  virtual bool			trylockMutex() = 0;
  virtual bool			unlockMutex() = 0;
};

#endif				//ABSTRACTMUTEX_HPP
