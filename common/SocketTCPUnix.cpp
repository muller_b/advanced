#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <cstring>
#include <cstdio>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include "SocketTCPUnix.hpp"
#include "SocketSelectUnix.hpp"

MySocket	SocketTCPUnix::getSocket() const
{
  return this->_sock;
}

SocketTCPUnix::SocketTCPUnix() : _ini(false)
{
  struct protoent*	proto;

  this->_connected = false;
  if ((proto = getprotobyname("tcp")) == NULL)
    {
      std::cerr << "[WARNING][TCP] getprotobyname() failed." << std::endl;
      return ;
    }
  if ((this->_sock = socket(AF_INET, SOCK_STREAM, proto->p_proto)) == -1)
    {
      std::cerr << "[WARNING][TCP] socket() creation failed." << std::endl;
      return ;
    }
  (SocketSelectUnix::getInst())->addSocket(this);
  if ((this->_sinfo = new struct sockaddr_in) == NULL)
    {
      std::cerr << "[WARNING][TCP] new(struct sockaddr_in) failed." << std::endl;
      return ;
    }
  this->_ini = true;
}

SocketTCPUnix::SocketTCPUnix(struct sockaddr_in* siadr, int sock) : _sock(sock), _ini(true), _sinfo(siadr)
{  
  this->_connected = false;
  (SocketSelectUnix::getInst())->addSocket(this);
}

bool		SocketTCPUnix::listen(const unsigned int port, const int maxcon)
{
  if (this->_ini)
    {
      this->_sinfo->sin_family = AF_INET;
      this->_sinfo->sin_port = htons(port);
      this->_sinfo->sin_addr.s_addr = htonl(INADDR_ANY);
      bzero(&(this->_sinfo->sin_zero), 8);
      if (bind(this->_sock, (struct sockaddr*)this->_sinfo, sizeof(struct sockaddr)) == -1)
	{
	  std::cerr << "[WARNING][TCP] bind() failed on port " << port << std::endl;
	  return false;
	}
      if (::listen(this->_sock, maxcon) == -1)
	{
	  std::cerr << "[WARNING][TCP] listen() failed." << std::endl;
	  return false;
	}
      return true;
    }
  return false;
}

bool		SocketTCPUnix::connect(const unsigned int port, const std::string& ip)
{
  if (this->_ini)
    {
      this->_sinfo->sin_family = AF_INET;
      this->_sinfo->sin_port = htons(port);
      this->_sinfo->sin_addr.s_addr = inet_addr(ip.c_str());
      bzero(&(this->_sinfo->sin_zero), 8);

      if (::connect(this->_sock, (struct sockaddr *)this->_sinfo, sizeof(struct sockaddr)) == -1)
	{
	  std::cerr << "[WARNING][TCP] Cant connect to " << ip << ":" << port << std::endl;
	  return false;
	}
      this->_connected = true;
      return true;
    }
  return false;
}

APISocketTCP*	SocketTCPUnix::accept()
{
  int			sock;
  socklen_t		unused;
  struct sockaddr_in*	siadr;
  APISocketTCP*		ntcps;

  if ((siadr = new struct sockaddr_in) == NULL)
    {
      std::cerr << "[WARNING][TCP] new(struct sockaddr_in) failed." << std::endl;
      return NULL;
    }
  unused = sizeof(struct sockaddr);
  if ((sock = ::accept(this->_sock, (struct sockaddr*)siadr, &unused)) >= 0)
    {
      ntcps = new SocketTCPUnix(siadr, sock);
      return ntcps;
    }
  std::cerr << "[WARNING][TCP] accept() failed." << std::endl;
  return NULL;
}

SocketTCPUnix::~SocketTCPUnix()
{
  if (close(this->_sock) == -1)
    std::cerr << "[WARNING][TCP] close(socket) failed." << std::endl;
  (SocketSelectUnix::getInst())->rmSocket(this);
}

bool		SocketTCPUnix::send(const void* data, size_t size)
{
  if ((::send(this->_sock, data, size, MSG_DONTWAIT)) == -1)
    {
      std::cerr << "[WARNING][TCP] send() failed." << std::endl;
      return false;
    }
  return true;
}

bool		SocketTCPUnix::recv(void* data, size_t size)
{
  int		ret;

  if ((ret = ::recv(this->_sock, data, size, MSG_DONTWAIT)) <= 0)
    {
      if (ret == 0)
	this->_connected = false;
      else
	std::cerr << "[WARNING][TCP] recv() failed" << std::endl;
      return false;
    }
  return true;
}

struct sockaddr_in*	SocketTCPUnix::getDest() const
{
  return this->_sinfo;
}

bool		SocketTCPUnix::connected() const
{
  return this->_connected;
}
