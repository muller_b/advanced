#ifndef _SOCKETTCPUNIX_H_
#define _SOCKETTCPUNIX_H_

#include <bits/types.h>
#include "APISocketTCP.hpp"

class SocketTCPUnix : public APISocketTCP
{
  int			_sock;
  bool			_ini;
  struct sockaddr_in*	_sinfo;
  bool			_connected;

  SocketTCPUnix(struct sockaddr_in* siadr, int sock);
public:
  SocketTCPUnix();
  ~SocketTCPUnix();

  struct sockaddr_in*	getDest() const;
  bool		listen(const unsigned int port, const int maxcon);
  bool		connect(const unsigned int port, const std::string& ip);
  bool		send(const void* data, size_t);
  bool		recv(void* data, size_t);
  bool		connected() const;
  APISocketTCP*	accept();
  MySocket	getSocket() const;
};

typedef SocketTCPUnix ASocketTCP;

#endif /* _SOCKETTCPUNIX_H_ */
