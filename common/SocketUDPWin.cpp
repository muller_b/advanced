#include		"SocketUDPWin.hpp"
#include		"SocketSelectWin.hpp"

SocketUDPWin::SocketUDPWin() : _ini(false)
{
  int			err;
  struct protoent*      proto;

  if ((proto = getprotobyname("udp")) == NULL)
    {
      std::cerr << "getprotobyname() failed." << std::endl;
      return ;
    }
  this->_wVersion = MAKEWORD(2, 2);
  err = WSAStartup(this->_wVersion, &this->_wsaData);
  if (err != 0)
    {
      std::cerr << "WSAStartup failed with error :" << err << std::endl;
      return ;
    }
  if (LOBYTE(this->_wsaData.wVersion) != 2 || HIBYTE(this->_wsaData.wVersion) != 2)
    {
      WSACleanup();
      return ;
    }
  this->_sock = WSASocket(AF_INET, SOCK_DGRAM, proto->p_proto, NULL, 0, WSA_FLAG_OVERLAPPED);
  if (this->_sock == INVALID_SOCKET)
    {
      std::cerr << WSAGetLastError() << std::endl;
      WSACleanup();
      return ;
    }
  (SocketSelectWin::getInst())->addSocket(this);
  if ((this->_sinfo = new struct sockaddr_in) == NULL)
    {
      std::cerr << "new(struct sockaddr_in) failed." << std::endl;
      return ;
    }
  ULONG fionbio = TRUE;
  ioctlsocket(this->_sock, FIONBIO, &fionbio); 
  this->_ini = true;
}

SocketUDPWin::~SocketUDPWin()
{
  if (closesocket(this->_sock) == SOCKET_ERROR)
    std::cerr << WSAGetLastError() << std::endl;
  if (WSACleanup() != 0)
    std::cerr << WSAGetLastError() << std::endl;
  //(SocketSelectUnix::getInst())->rmSocket(this); ??
}

SocketUDPWin::SocketUDPWin(struct sockaddr_in* sinfo) : _ini(false), _sinfo(sinfo)
{
  /*
  struct protoent*      proto;

  if ((proto = getprotobyname("udp")) == NULL)
    {
      std::cerr << "getprotobyname() failed." << std::endl;
      return ;
    }
  */
  this->_sock = WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, NULL, 0, WSA_FLAG_OVERLAPPED);
  if (this->_sock == INVALID_SOCKET)
    {
      std::cerr << WSAGetLastError() << std::endl;
      WSACleanup();
      return ;
    }
  //(SocketSelectWin::getInst())->addSocket(this);
  this->_ini = true;
}

void			SocketUDPWin::setDest(const std::string& ip, const unsigned int port)
{
  this->_sinfo->sin_family = AF_INET;
  this->_sinfo->sin_port = htons(port);
  inet_pton(this->_sinfo->sin_family, ip.c_str(), &(this->_sinfo->sin_addr.s_addr));
  memset(&(this->_sinfo->sin_zero), 0, 8);
}

void			SocketUDPWin::setDest(const struct sockaddr_in* sinfo)
{
  memcpy(const_cast<void*>(static_cast<const void*>(sinfo)), this->_sinfo, sizeof(struct sockaddr_in));
}

struct sockaddr_in*	SocketUDPWin::getSender() const
{
  return this->_senderinfo;
}

bool			SocketUDPWin::send(const void* data, size_t nbBytes)
{
  if ((::sendto(this->_sock, static_cast<const char*>(data), nbBytes, 0, (struct sockaddr*)this->_sinfo, sizeof(struct sockaddr))) == -1)
    {
      std::cerr << "Win send() failed." << std::endl;
      return false;
    }
  return true;
}

bool			SocketUDPWin::recv(void* data, size_t nbBytes)
{
  socklen_t     len;

  if ((::recvfrom(this->_sock, static_cast<char *>(data), nbBytes, 0, (struct sockaddr*)this->_senderinfo, &len)) == -1)
    {
      std::cerr << "Win recv() failed" << std::endl;
      return false;
    }
  return true;
}

MySocket	       	SocketUDPWin::getSocket() const
{
  return this->_sock;
}

bool			SocketUDPWin::bind(const unsigned int port)
{
  if (this->_ini)
    {
      this->_sinfo->sin_family = AF_INET;
      this->_sinfo->sin_port = htons(port);
      this->_sinfo->sin_addr.s_addr = htonl(INADDR_ANY);
      memset(&(this->_sinfo->sin_zero), 0, 8);
      
      if (::bind(this->_sock, (struct sockaddr*)this->_sinfo, sizeof(struct sockaddr)) == -1)
        {
          std::cerr << "bind() failed on port " << port << std::endl;
          return false;
        }
    }
  return true;
}
