#ifndef _SOCKETUDPUNIX_H_
#define _SOCKETUDPUNIX_H_

#include "APISocketUDP.hpp"
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <cstring>
#include <cstdio>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>

class SocketUDPUnix : public APISocketUDP
{
  int			_sock;
  bool			_ini;
  struct sockaddr_in*	_sinfo;
  struct sockaddr_in*	_senderinfo;

public:
  SocketUDPUnix(struct sockaddr_in* sinfo);
  SocketUDPUnix();
  ~SocketUDPUnix();

  void		setDest(const std::string& ip, const unsigned int port);
  void		setDest(const struct sockaddr_in* sinfo);
  struct sockaddr_in*	getSender() const;

  bool		send(const void* data, size_t);
  bool		recv(void* data, size_t);
  MySocket	getSocket() const;
  bool		bind(const unsigned int port);
};

typedef SocketUDPUnix ASocketUDP;

#endif /* _SOCKETUDPUNIX_H_ */
