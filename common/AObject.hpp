#ifndef		H_AOBJECT
#define		H_AOBJECT

#include	"Vecteur.hpp"

class AObject
{
protected:
  Vecteur	_pos;
  Vecteur	_dim;
  Vecteur	_dir;
  int		_id;
  int		_lv;
  int		_life;
  int		_type;

public:
  AObject(int id);
  ~AObject();
  virtual void	 collision(AObject*);
  virtual void	 keepDir();
  virtual void	 deplacement(const Vecteur&);
  virtual void	 deplacement(int x, int y);
  virtual Vecteur const& getDim() const;
  virtual Vecteur const& getDir() const;
  virtual Vecteur const& getPos() const;
  virtual void	 setDim(int x, int y);
  virtual void	 setDir(int x, int y);
  virtual void	 setPos(int x, int y);
  virtual void	 setDir(const Vecteur);
  virtual void	 setDim(const Vecteur);
  virtual void	 setPos(const Vecteur);
  virtual int	 getId() const;
  virtual void	 setId(int);
  virtual int	 getLv() const;
  virtual int	 getLife() const;
  virtual void	 setLv(int);
  virtual void	 setLife(int);
  virtual void	 setType(int type);
  virtual int	 getType() const;
};

#endif		// !H_AOBJECT
