#include "UnixDL.hpp"

ISharedLib::dl		UnixDL::ldl(const std::string& file)
{
  return (static_cast<dl>(dlopen(file.c_str(), RTLD_NOW)));
}

ISharedLib::result	UnixDL::fdl(dl lib)
{
  return (static_cast<result>(dlclose(lib)));
}

ISharedLib::sym		UnixDL::getSym(dl lib, const std::string& name)
{
  return (static_cast<sym>(dlsym(lib, static_cast<const char*>(name.data()))));
}
