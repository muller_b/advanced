#ifndef			WINMUTEX_HPP__
#define			WINMUTEX_HPP__
#include		"APIMutex.hpp"

class			MutexWindows : public APIMutex
{
public:
  MutexWindows();
  virtual ~MutexWindows();
  virtual bool		lockMutex();
  virtual bool		trylockMutex();
  virtual bool		unlockMutex();
private:
  mutex			_myMutex;
};

typedef MutexWindows AMutex;

#endif			//WINMUTEX_HPP
