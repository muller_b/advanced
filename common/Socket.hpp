#ifndef _SOCKET_H_
#define _SOCKET_H_

#ifdef			_WIN32
# include		"SocketTCPWin.hpp"
# include		"SocketUDPWin.hpp"
# include		"SocketSelectWin.hpp"
#else
# include		"SocketTCPUnix.hpp"
# include		"SocketUDPUnix.hpp"
# include		"SocketSelectUnix.hpp"
#endif			/*_WIN32*/


#endif /* _SOCKET_H_ */
