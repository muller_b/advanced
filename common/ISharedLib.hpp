#ifndef		__ISHAREDLIB_H__
# define	__ISHAREDLIB_H__

#include <iostream>

class		ISharedLib
{
public:
#ifndef		_WIN32
  typedef		void*		sym;
  typedef		int		result;
  typedef		void*		dl;
#else
  typedef		void*		sym;
  typedef		int		result;
  typedef		void*		dl;
#endif

public:
  virtual dl		ldl(const std::string&) = 0;
  virtual result	fdl(dl) = 0;
  virtual sym		getSym(dl, const std::string&) = 0;
};

#endif
