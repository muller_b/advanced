#ifndef		__WINDOWSDLL_H__
# define	__WINDOWSDLL_H__

#include <iostream>
#include "ISharedLib.hpp"

class		WindowsDLL : public ISharedLib
{
public:
	dl		ldl(const std::string&);
	result	fdl(dl);
	sym		getSym(dl, const std::string&);
};

#endif
