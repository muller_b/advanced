#ifndef			UNIXMUTEX_HPP__
#define			UNIXMUTEX_HPP__
#include	       	"APIMutex.hpp"
#include		<stdexcept>

class			MutexUnix : public APIMutex
{
public:
  MutexUnix();
  virtual ~MutexUnix();
  virtual bool		lockMutex();
  virtual bool	       	trylockMutex();
  virtual bool		unlockMutex();
private:
  mutex			_myMutex;
};

typedef MutexUnix AMutex;

#endif			//UNIXMUTEX_HPP
