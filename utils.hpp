#ifndef		_UTILS_HPP_
#define		_UTILS_HPP_

#ifdef _WIN32

#include <Windows.h>

typedef		UINT8	Uint8;
typedef		UINT16	Uint16;
typedef		UINT32	Uint32;

#endif // _WIN32

int***			createGaussFilters(int maxFilterSize);
int**			createGaussFilter(int filterSize);


#endif
