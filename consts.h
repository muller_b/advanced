#ifndef		_CONSTS
#define		_CONSTS

#ifdef _WIN32
	#include <SDL.h>
#else
	#include <SDL/SDL.h>
#endif

const int	_xdim = 1600;
const int	_ydim = 1000;
const int	_bpp = 32;
const int	_nbObj = 16;
const unsigned int	_nbCases = _xdim * _ydim;
const int	_verb = 0;

#endif
