#ifndef			_GAME
#define			_GAME

#ifdef _WIN32
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#endif
#include <map>
#include <list>
#include "consts.h"

unsigned int    randuint(unsigned int max);

typedef struct		s_coord
{
	long double		x;
	long double		y;
}			Coord;

typedef struct		s_opts
{

	bool		usingBlur = false;
	Uint8		rOfs = 0;
	Uint8		vOfs = 0;
	Uint8		bOfs = 0;

	bool		usingFakeAA = false;

	int			AALevel = 1;
	bool		usingRealAA = true;
	int			maxAALevel = 3;
	int***		gaussFilters;
}			opts;

class			Game
{
public:
	static const int	TITLE_BUFFER_SIZE = 256;

	opts			_options;
	bool			_refreshTitle = false;
	bool			_requireFullRedraw = false;
private:
	SDL_Surface*	_screenSurface;
	SDL_Window*		_screen;
	SDL_Renderer*	_screenRenderer;
	SDL_Texture*	_screenTexture;
	Coord			_cam;
	long long		_zoom;
	Coord			_mouse;
	int				_clicked;
	const Uint8*	_keystate;
	bool			_paused;
	int				_edges;
	TTF_Font*		_font;
	char			_title[TITLE_BUFFER_SIZE];
	bool			_ready;

	Coord			_mouseLeftDown;

	void			switchAA();
	void			switchBlur();
	void			modR(Uint8 val);
	void			modV(Uint8 val);
	void			modB(Uint8 val);
	void			cam_moveRatio(int x, int y);
	void			cam_move(int x, int y);
	void			manage_mouse(SDL_Event*);
	int				manage_keyboard();
	void			applyCameraZoom(Coord& cursorPos, long long newZoom);
	void			updateMousePos();
public:
	Game(int);

	void			increaseCam();
	bool			isReady() const;

	bool			belongs(int, int);
	void			font_load();
	void			text(const char* t, SDL_Color* c, int, int);
	bool			init_SDL();
	void			cls();
	int				timer_countdown(int *start, int seconds);
	void			setEdges(int);
	int				getEdges() const;
	bool			paused() const;
	int				get_timer();
	char*			getTitle();
	void			setTitle(const char* title);
	void			applyTitle();
	SDL_Surface*	getScreen() const;
	long long		getZoom() const;
	Coord			getCam() const;
	bool			clicked() const;
	int				getMx() const;
	int				getMy() const;
	void			wait();
	int				manage_keys(bool allowKeyRepeat);

	void			blur();
	void			centeredSquare32(Uint16 x, Uint16 xSize, Uint16 y, Uint16 ySize, Uint32 color);
	void			square32(Uint16 x, Uint16 xSize, Uint16 y, Uint16 ySize, Uint32 color);
	void			dot32(Uint16 x, Uint16 y, Uint32 col);
	void			dot32(SDL_Surface* dst, Uint16 x, Uint16 y, Uint32 col);
	Uint32			getDot32(SDL_Surface* dst, Uint16 x, Uint16 y);
	void			maxDot32(SDL_Surface* dst, Uint16 x, Uint16 y, Uint32 col);
	void			dot(Uint16 x, Uint16 y, Uint32 col);
	void			sync();
};

#endif
